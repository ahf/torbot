# Copyright (c) 2020 The Tor Project, inc. All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

import irc3
from aiohttp import web

routes = web.RouteTableDef()

@routes.get("/")
async def hello(request):
    return web.Response(text="Hello World!")


@irc3.plugin
class Http:
    def __init__(self, bot):
        enabled = bot._config.http_enabled()

        if not enabled:
            return

        unix_socket_path = bot._config.http_unix_socket()

        bot.log.info("Starting HTTP interface on {}".format(unix_socket_path))

        app = web.Application()
        app.add_routes(routes)
        app["bot"] = bot

        self._server = bot.create_task(bot.loop.create_unix_server(app.make_handler(), unix_socket_path))
