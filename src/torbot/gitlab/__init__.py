# Copyright (c) 2020 The Tor Project, inc. All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

import gitlab

import irc3
import irc3.rfc

import re
import sys

from typing import List, Optional


class GitlabScanner:
    def __init__(self, server_name: str):
        self._types = []

        escaped_server_name = server_name.replace(".", "\.")

        for (character, keyword) in [("#", "issues"), ("!", "merge_requests")]:
            self._types.append((character, re.compile(r"([\w\/-]+)?" + character + r"([0-9]+)")))
            self._types.append((character, re.compile(escaped_server_name + r"\/([\w\/-]*)\/-\/" + keyword + r"\/(\d+)")))

    def match(self, input_string: str) -> List[str]:
        result = []

        for type_name, regex in self._types:
            for match in regex.finditer(input_string):
                project_name = match.group(1)
                identifier = match.group(2)

                result.append({
                    "type": type_name,
                    "project": project_name,
                    "identifier": int(identifier),
                })

        return result

class FuzzyProjectFinder:
    # preference is a list of projects to prefer in case of tie
    def __init__(self, projects: List[str], preferences: List[str]) -> None:
        self._projects = {}
        self._preferences = preferences

        projects.sort()

        for project in projects:
            tokens = project.split('/')
            tokens.reverse()

            assert project not in self._projects
            self._projects[project] = tokens

    def match(self, input_project: str, default_project: str) -> List[str]:
        if input_project is None:
            if default_project is None:
                return []

            input_project = default_project

        input_tokens = input_project.split('/')
        input_tokens.reverse()
        input_tokens_len = len(input_tokens)

        result = []
        preferred_result = []

        for project_name, project_tokens in self._projects.items():
            project_tokens = project_tokens.copy()

            while project_tokens:
                if input_tokens == project_tokens[0:input_tokens_len]:
                    result.append(project_name)
                    if project_name in self._preferences:
                        preferred_result.append(project_name)
                    break

                project_tokens.pop()

        if len(preferred_result) == 1:
            return preferred_result
        else:
            return result

@irc3.plugin
class Gitlab:
    def __init__(self, bot):
        self.bot = bot
        self.gitlab_client = gitlab.Gitlab(bot._config.gitlab_server(),
                                           private_token=bot._config.gitlab_access_token(),
                                           api_version=4)
        print("Authenticating with Gitlab")
        self.gitlab_client.auth()

        self.scanner = GitlabScanner(bot._config.gitlab_server())

        self.projects = {}
        self.update_project_list()

    def update_project_list(self):
        print("Updating project list")
        projects = {}

        for project in self.gitlab_client.projects.list(all=True):
            if project.path_with_namespace.startswith("tpo/"):
                projects[project.path_with_namespace] = project

        self.projects = projects

        print("Keys: {}".format(projects.keys()))

    @irc3.event(irc3.rfc.PRIVMSG)
    def on_privmsg(self, mask, event, target, data):
        if event == 'NOTICE' or data.startswith('\x01VERSION') or not target.is_channel:
            return

        finder = FuzzyProjectFinder(
                list(self.projects.keys()),
                self.bot._config.preferred_projects(target)
                )

        for match in self.scanner.match(data):
            matches = finder.match(match["project"], self.bot._config.default_project(target))

            if len(matches) == 0:
                # self.bot.privmsg(target, "Unknown project: {}".format(match["project"]))
                continue
            elif len(matches) == 1:
                project_name = list(matches)[0]
                project_type = match["type"]
                project_id   = int(match["identifier"])

                project = self.projects.get(project_name, None)

                if project is None:
                    continue

                try:
                    if project_type == "#":
                        issue = project.issues.get(project_id)
                        print(issue)

                        self.bot.notice(target, "{} - {}".format(issue.web_url, issue.title))
                    elif project_type == "!":
                        mr = project.mergerequests.get(project_id)
                        print(mr)

                        self.bot.notice(target, "{} - {}".format(mr.web_url, mr.title))
                    else:
                        print("Ugh? {}".format(matches))
                        continue
                except Exception as e:
                    print("Exception: {}".format(e))

            else:
                self.bot.privmsg(target, "Uhm, which one of [{}] did you mean?".format(", ".join(matches)))

