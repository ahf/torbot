# Copyright (c) 2020 The Tor Project, inc. All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

import logging

def initialize_logging(config):
    if not config.log_enabled():
        return

    logging.basicConfig(level=config.log_level(),
                        format="%(asctime)s %(levelname)s: %(message)s",
                        datefmt="%Y/%m/%d %H:%M:%S")
