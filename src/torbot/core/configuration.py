# Copyright (c) 2020 The Tor Project, inc. All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

import os.path

from yaml import load, dump

try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper


class ConfigurationError(Exception):
    pass


class Configuration:
    def __init__(self, config):
        self._config = config
        self._gitlab_config = None

    def log_enabled(self) -> bool:
        return self.get("log.enabled")

    def log_level(self) -> str:
        return self.get("log.level")

    def irc_enabled(self) -> bool:
        return self.get("irc.enabled")

    def irc_client_nickname(self) -> str:
        return self.get("irc.client.nickname")

    def irc_client_username(self) -> str:
        return self.get("irc.client.username")

    def irc_client_realname(self) -> str:
        return self.get("irc.client.realname")

    def irc_server_hostname(self) -> str:
        return self.get("irc.server.hostname")

    def irc_server_port(self) -> int:
        return self.get("irc.server.port")

    def irc_server_tls(self) -> bool:
        return self.get("irc.server.tls")

    def irc_server_tls_auth_cert(self) -> str:
        return os.path.expanduser(self.get("irc.server.tls_auth_cert"))

    def irc_server_tls_auth_key(self) -> str:
        return os.path.expanduser(self.get("irc.server.tls_auth_key"))

    def irc_channels(self):
        return self.get("irc.channels")

    def http_enabled(self) -> bool:
        return self.get("http.enabled")

    def http_unix_socket(self) -> str:
        return os.path.expanduser(self.get("http.unix_socket"))

    def gitlab_server(self) -> str:
        return self.get("gitlab.server")

    def gitlab_access_token_file(self):
        if self._gitlab_config is not None:
            return self._gitlab_config

        gitlab_config = os.path.expanduser(self.get("gitlab.access_token_file"))

        if not os.path.isfile(gitlab_config):
            raise ConfigurationError("gitlab access_token_file missing")

        data = None

        try:
            with open(gitlab_config) as f:
                data = load(f.read(), Loader=Loader)
        except Exception as e:
            raise ConfigurationError(e)

        if "access_token" not in data:
            raise ConfigurationError("missing access_token in {}".format(gitlab_config))

        self._gitlab_config = data
        return self._gitlab_config

    def gitlab_access_token(self) -> str:
        data = self.gitlab_access_token_file()
        return data.get("access_token", "")

    def preferred_projects(self, target: str):
        for channel in self.irc_channels():
            if channel["channel"] != target:
                continue
            return channel.get("preferred_projects") or []
        return []

    def default_project(self, target: str):
        for channel in self.irc_channels():
            if channel["channel"] != target:
                continue
            return channel.get("default_project")
        return None

    def get(self, key: str):
        tokens = key.split(".")
        config = self._config

        for token in tokens:
            config = config.get(token, None)

            if config is None:
                raise ConfigurationError("Missing key: {}".format(key))

        return config

    @staticmethod
    def from_config_directory(path: str):
        main_config = os.path.join(path, "main.yaml")

        if not os.path.isfile(main_config):
            raise ConfigurationError("main.yaml missing")

        data = None

        try:
            with open(main_config) as f:
                data = load(f.read(), Loader=Loader)
        except Exception as e:
            raise ConfigurationError(e)

        return Configuration(data)
